//
//  Earthquake.h
//  EarthquakesMobile
//
//  Created by User on 3/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Earthquake : NSManagedObject

@property (nonatomic, retain) NSString * place;
@property (nonatomic, retain) NSString * magnitudeType;
@property (nonatomic, retain) NSNumber * magnitude;
@property (nonatomic, retain) NSNumber * latitude;
@property (nonatomic, retain) NSNumber * longitude;
@property (nonatomic, retain) NSNumber * tsunami;

@end
