//
//  EMEarthquakeDetailViewController.m
//  EarthquakesMobile
//
//  Created by User on 3/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import "EMEarthquakeDetailViewController.h"
#import "STTwitter/STTwitter.h"
#import "EMTweetTableViewCell.h"
#import "AFNetworking/UIKit+AFNetworking.h"

@interface EMEarthquakeDetailViewController ()

@property Earthquake* earthquake;
@property NSArray* tweets;
@property (nonatomic, strong)  EMTweetTableViewCell* prototypeCell;

@property UIBarButtonItem* barButtonItem;
@property bool mapShown;

@end

@implementation EMEarthquakeDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil earthquake: (Earthquake*) earthquake;
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.earthquake = earthquake;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.label.text = self.earthquake.place;
    
    CLLocationCoordinate2D coords;
    coords.latitude = [self.earthquake.latitude floatValue];
    coords.longitude = [self.earthquake.longitude floatValue];
    self.mapView.centerCoordinate = coords;
    self.mapView.zoomEnabled = YES;
    self.mapView.pitchEnabled = YES;
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.coordinate = coords;
    
    [self.mapView addAnnotation:annotation];
    
    // Navigation bar
    self.navigationItem.title = @"Earthquake";
    self.barButtonItem =
        [[UIBarButtonItem alloc] initWithTitle:@"Hide map" style:UIBarButtonItemStylePlain target:self action:@selector(switchMap)];
    
    self.navigationItem.rightBarButtonItem = self.barButtonItem;
    
    self.mapShown = true;
    
    // Twitter
    
    self.tweets = [[NSArray alloc] init];
    
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"duExUuIbmeDUA1WgEoQWePFbx"
                                                            consumerSecret:@"TJpuN0bNq0iPc1SrnHz6dMQcjIwKesm1Bjs8pbhtN51YvpYV7D"];
    
    [twitter verifyCredentialsWithSuccessBlock:^(NSString *bearerToken) {
        
        NSString* geocode = [NSString stringWithFormat:@"%f,%f,25km", [self.earthquake.latitude floatValue], [self.earthquake.longitude floatValue]];
        
        [twitter getSearchTweetsWithQuery:@"earthquake" geocode:geocode lang:nil locale:nil resultType:nil count:@"20" until:nil sinceID:nil maxID:nil includeEntities:nil callback:nil successBlock:^(NSDictionary *searchMetadata, NSArray *statuses) {
            
            self.tweets = statuses;

            [self.tableView reloadData];
            
            [self hideLoader];
            
        } errorBlock:^(NSError *error) {
            
            [self hideLoader];
        }];
        
    } errorBlock:^(NSError *error) {
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Can't download tweets"
                                                          message:@"Internet connection problem"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        [self hideLoader];
    }];
    
}

- (void)hideLoader
{
    [UIView animateWithDuration:0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         self.loaderView.alpha = 0.0f;
                     }
                     completion:^(BOOL finished){
                         [self.loaderView removeFromSuperview];
                     }];
}

- (void)switchMap
{
    if (self.mapShown)
    {
        self.mapHeightConstraint.constant = 64.0f;
        self.barButtonItem.title = @"Show map";
    }
    else
    {
        self.mapHeightConstraint.constant = 240.0f;
        self.barButtonItem.title = @"Hide map";
    }
    
    [UIView animateWithDuration:0.3
                          delay: 0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
    
    self.mapShown = !self.mapShown;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.tweets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EMTweetTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TweetCell"];
    
    if (cell == nil)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EMTweetTableViewCell" owner:self options:nil];
        cell = [nib objectAtIndex:0];
    }
    
    [self configureCell:cell forRowAtIndexPath:indexPath];
    
    return cell;
}

- (EMTweetTableViewCell *) prototypeCell
{
    if (!_prototypeCell)
    {
        NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"EMTweetTableViewCell" owner:self options:nil];
        _prototypeCell = [nib objectAtIndex:0];
    }
    return _prototypeCell;
}

- (void)configureCell:(EMTweetTableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary* tweet = (NSDictionary*)[self.tweets objectAtIndex:indexPath.row];
    NSDictionary* user = tweet[@"user"];
    
    if (cell.image.image == nil)
    {
        [cell.image setImageWithURL:[NSURL URLWithString:user[@"profile_image_url"]]];
    }
    
    cell.name.text = user[@"name"];
    cell.screenName.text = [NSString stringWithFormat: @"@%@", user[@"screen_name"]];
    cell.text.text = tweet[@"text"];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self configureCell:self.prototypeCell forRowAtIndexPath:indexPath];
    [self.prototypeCell layoutIfNeeded];
    
    CGSize size = [self.prototypeCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height+1;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    NSDictionary* tweet = (NSDictionary*)[self.tweets objectAtIndex:indexPath.row];
    
    NSString* twitterURLString = [NSString stringWithFormat:@"twitter://status?id=%@", tweet[@"id"]];
    
    NSURL *twitterURL = [NSURL URLWithString:twitterURLString];
    if ([[UIApplication sharedApplication] canOpenURL:twitterURL])
    {
        [[UIApplication sharedApplication] openURL:twitterURL];
    }
    else
    {
        twitterURLString = [NSString stringWithFormat:@"http://twitter.com/1Direction_X/statuses/%@", tweet[@"id"]];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:twitterURLString]];
    }
}



@end
