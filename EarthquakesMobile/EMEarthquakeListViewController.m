//
//  EMEarthquakeListViewController.m
//  EarthquakesMobile
//
//  Created by User on 3/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import "EMEarthquakeListViewController.h"
#import "EMAppDelegate.h"
#import "EMEarthquakeDetailViewController.h"

#import "Earthquake.h"
#import "AFNetworking/AFNetworking.h"

@interface EMEarthquakeListViewController ()

@property EMAppDelegate* APP;
@property NSArray *earthquakes;
@property NSArray *earthquakesTsunami;
@property NSArray *earthquakesDisplayed;

// Navigation items
@property UIBarButtonItem* barButtonLoading;
@property UIBarButtonItem* barButtonRefresh;

@end

@implementation EMEarthquakeListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.APP = (EMAppDelegate *)[[UIApplication sharedApplication] delegate];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.title = @"Latest earthquakes";
    
    self.barButtonRefresh = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemRefresh target:self action:@selector(synchronizeEarthquakes)];
    
    UIActivityIndicatorView* activityIndicator  = [[UIActivityIndicatorView alloc] init];
    activityIndicator.color = [UIColor blackColor];
    [activityIndicator startAnimating];
    
    self.barButtonLoading = [[UIBarButtonItem alloc] initWithCustomView:activityIndicator];
    self.navigationItem.rightBarButtonItem = self.barButtonRefresh;
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    [self reloadTableView];
    
    [self synchronizeEarthquakes];
}

- (void) reloadTableView
{
    NSFetchRequest* fetchRequest = [[NSFetchRequest alloc] init];
    NSSortDescriptor *sortDescriptorMagnitude = [[NSSortDescriptor alloc]
                                        initWithKey:@"magnitude" ascending:NO];
    NSSortDescriptor *sortDescriptorPlace = [[NSSortDescriptor alloc]
                                        initWithKey:@"place" ascending:YES];
    [fetchRequest setSortDescriptors:@[sortDescriptorMagnitude, sortDescriptorPlace]];
    
    NSEntityDescription* entity = [NSEntityDescription entityForName:@"Earthquake" inManagedObjectContext:[self.APP managedObjectContext]];
    [fetchRequest setEntity:entity];
    self.earthquakes = [[self.APP managedObjectContext] executeFetchRequest:fetchRequest error:nil];
    self.earthquakesTsunami = [self.earthquakes filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"SELF.tsunami == YES"]];
    self.earthquakesDisplayed = self.earthquakes;
    
    self.filterView.selectedSegmentIndex = 0;
    
    [self.tableView reloadData];
}

- (void) synchronizeEarthquakes
{
    [self.navigationItem setRightBarButtonItem:self.barButtonLoading];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:@"http://earthquake.usgs.gov/earthquakes/feed/geojson/2.5/week" parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSArray *earthquakes = responseObject[@"features"];
        
        // Sort by magnitudes
        NSArray* earthquakesSorted = [earthquakes sortedArrayUsingComparator:^(NSDictionary* e1, NSDictionary* e2) {
            
            NSDictionary* properties1 = [e1 objectForKey:@"properties"];
            NSNumber* mag1 = [properties1 objectForKey:@"mag"];
            
            NSDictionary* properties2 = [e2 objectForKey:@"properties"];
            NSNumber* mag2 = [properties2 objectForKey:@"mag"];
            
            return [mag2 compare:mag1];
        }];
        
        // Delete old earthquakes
        for(Earthquake *earthquake in self.earthquakes)
        {
            [[self.APP managedObjectContext] deleteObject:earthquake];
        }
        
        // Add new earthquakes
        int counter = 0;
        for(NSDictionary* earthquake in earthquakesSorted)
        {
            NSDictionary* properties = [earthquake objectForKey:@"properties"];
            NSDictionary* geometry = [earthquake objectForKey:@"geometry"];
            NSArray* coordinates = [geometry objectForKey:@"coordinates"];
            
            Earthquake *newEarthquake = [NSEntityDescription
                                         insertNewObjectForEntityForName:@"Earthquake"
                                         inManagedObjectContext:[self.APP managedObjectContext]];
            
            newEarthquake.place = [properties objectForKey:@"place"];
            newEarthquake.magnitude = [properties objectForKey:@"mag"];
            newEarthquake.magnitudeType = [properties objectForKey:@"magnitudeType"];
            newEarthquake.latitude = [coordinates objectAtIndex:1];
            newEarthquake.longitude = [coordinates objectAtIndex:0];
            newEarthquake.tsunami = ([properties objectForKey:@"tsunami"] == [NSNull null]) ? [NSNumber numberWithBool:NO] : [NSNumber numberWithBool:YES];
            
            counter++;
            if (counter >= 20) break;
        }

        [self.APP saveContext];
        [self reloadTableView];
        
        [self.navigationItem setRightBarButtonItem:self.barButtonRefresh animated:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        UIAlertView *message = [[UIAlertView alloc] initWithTitle:@"Earthquakes can't be updated"
                                                          message:@"Internet connection problem"
                                                         delegate:nil
                                                cancelButtonTitle:@"OK"
                                                otherButtonTitles:nil];
        
        [message show];
        
        [self.navigationItem setRightBarButtonItem:self.barButtonRefresh animated:YES];
    }];
}

- (IBAction) onFilterChange: (id)sender
{
    UISegmentedControl* filter = (UISegmentedControl*) sender;
    if (filter.selectedSegmentIndex == 0)
    {
        self.earthquakesDisplayed = self.earthquakes;
    }
    else
    {
        self.earthquakesDisplayed = self.earthquakesTsunami;
    }
    
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableView Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.earthquakesDisplayed count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EarthquakeCell"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"EarthquakeCell"];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    Earthquake *earthquake = [self.earthquakesDisplayed objectAtIndex:indexPath.row];
    
    cell.detailTextLabel.text = earthquake.place;
    cell.textLabel.text = [NSString stringWithFormat:@"%@", earthquake.magnitude];

    return cell;
}

#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    Earthquake* earthquake = (Earthquake*)[self.earthquakesDisplayed objectAtIndex:indexPath.row];
    
    EMEarthquakeDetailViewController *detailViewController = [[EMEarthquakeDetailViewController alloc] initWithNibName:@"EMEarthquakeDetailViewController" bundle:nil earthquake:earthquake];
    
    [self.navigationController pushViewController:detailViewController animated:YES];
}


@end
