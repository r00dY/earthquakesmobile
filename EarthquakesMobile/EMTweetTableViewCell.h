//
//  EMTweetTableViewCell.h
//  EarthquakesMobile
//
//  Created by User on 4/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EMTweetTableViewCell : UITableViewCell

@property IBOutlet UIImageView* image;
@property IBOutlet UILabel* name;
@property IBOutlet UILabel* screenName;
@property IBOutlet UILabel* text;

@end
