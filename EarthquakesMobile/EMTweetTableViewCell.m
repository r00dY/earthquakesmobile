//
//  EMTweetTableViewCell.m
//  EarthquakesMobile
//
//  Created by User on 4/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import "EMTweetTableViewCell.h"

@interface EMTweetTableViewCell ()

@property(nonatomic, readwrite, copy) NSString *reuseIdentifier;

@end

@implementation EMTweetTableViewCell

@synthesize reuseIdentifier = _myCustomCellReuseIdentifier;

- (void)awakeFromNib
{
    self.reuseIdentifier = @"TweetCell";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
