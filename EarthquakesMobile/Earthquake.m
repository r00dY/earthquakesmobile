//
//  Earthquake.m
//  EarthquakesMobile
//
//  Created by User on 3/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import "Earthquake.h"


@implementation Earthquake

@dynamic place;
@dynamic magnitudeType;
@dynamic magnitude;
@dynamic latitude;
@dynamic longitude;
@dynamic tsunami;

@end
