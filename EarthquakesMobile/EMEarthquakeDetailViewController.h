//
//  EMEarthquakeDetailViewController.h
//  EarthquakesMobile
//
//  Created by User on 3/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "Earthquake.h"

@interface EMEarthquakeDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil earthquake: (Earthquake*) earthquake;

@property IBOutlet MKMapView* mapView;
@property IBOutlet UILabel* label;
@property IBOutlet UITableView *tableView;
@property IBOutlet UIView* loaderView;

@property IBOutlet NSLayoutConstraint* mapHeightConstraint;

@end
