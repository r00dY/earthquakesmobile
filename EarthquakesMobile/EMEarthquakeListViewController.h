//
//  EMEarthquakeListViewController.h
//  EarthquakesMobile
//
//  Created by User on 3/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EMEarthquakeListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>

@property IBOutlet UITableView* tableView;
@property IBOutlet UISegmentedControl* filterView;

- (IBAction) onFilterChange: (id)sender;

@end
