//
//  main.m
//  EarthquakesMobile
//
//  Created by User on 3/09/2014.
//  Copyright (c) 2014 Hello Universe. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "EMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([EMAppDelegate class]));
    }
}
